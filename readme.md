# Install

```
git clone https://gitlab.com/mo_krauti/card10-firmware-updater.git
cd card10-firmware-updater
chmod +x update.sh
```

# Run

```
sudo ./update.sh {firmware-zip-url} {card10-device}
```
For example:
```
sudo ./update.sh https://card10.badge.events.ccc.de/release/card10-v1.10-JerusalemArtichoke.zip /dev/sdb
```
