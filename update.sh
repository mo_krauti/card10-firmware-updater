path=/tmp
wget -O $path/firmware.zip $1
mkdir $path/firmware
unzip $path/firmware.zip -d $path/firmware
rm $path/firmware.zip
mkdir $path/mnt
mount $2 $path/mnt
cp -r $path/firmware/*/* $path/mnt
sync
umount $path/mnt
rm -r $path/firmware
rm -r $path/mnt
